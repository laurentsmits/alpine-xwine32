FROM i386/alpine:edge

MAINTAINER Laurent Smits "https://bitbucket.org/laurentsmits/alpine-x11"

LABEL description="Minimal alpine 32bit xserver - wine 2.0"

#Upgrade Alpine linux
RUN apk update upgrade

#APK Community
RUN apk --no-cache add xvfb openbox xfce4-terminal supervisor sudo alpine-base xvfb ncurses curl zenity wine

#APK Testing
RUN apk --no-cache add --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing x11vnc winetricks mono 

#Create alpine group ad user
RUN addgroup alpine \
  && adduser  -G alpine -s /bin/sh -D alpine \
  && echo "alpine:alpine" | /usr/sbin/chpasswd \
  && echo "alpine    ALL=(ALL) ALL" >> /etc/sudoers \
  && rm -rf /tmp/* /var/cache/apk/*

ADD ./etc /etc

ENV DISPLAY :0
ENV WINEPREFIX /home/alpine/.wine-x86
ENV WINEARCH=win32

EXPOSE 5900

USER alpine

WORKDIR /home/alpine

CMD ["/usr/bin/supervisord","-c","/etc/supervisord.conf"]